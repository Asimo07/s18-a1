console.log("Hello World")

let trainer = {
	name: 'Ash Ketchum',
	age: '10',
	friends: {
		hoenn : ['May', 'Max'],
		kanto : ['Brock', 'Misty']
	},
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	talk: function() {
		console.log(this.pokemon[0] + '!' +' I choose you!');
	}
}
console.log(trainer);
console.log('Result of dot notation:');
console.log(trainer.name);

console.log('Result of square bracket notation: ');
console.log(trainer['pokemon']);

console.log('Result of talk method');
trainer.talk();

function Pokemon(name, level){
	// Properties
	this.name = name
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + 'tackled' + ' ' + target.name);
		console.log(target.name + "'s health is now reduced to 16");
	}
	this.faint = function(){
		console.log(this.name + ' fainted.');	
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

geodude.tackle(pikachu);	

